<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/x-icon" href="hospital.ico"/>

    <title>Dr.V-FET e-Learning</title>

    <?php include 'protected/include/css.php';?>

</head>

<body>

    <div class="body">
        <?php include 'protected/include/header.php';?>
        <?php include 'protected/include/content.php';?>
        <?php include 'protected/include/footer.php';?>

    </div>

    <?php include 'protected/include/javascript.php';?>


</body>

</html>