<!-- Vendor -->
<script src="themes/template/vendor/jquery/jquery.min.js"></script>
<script src="themes/template/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="themes/template/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="themes/template/vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="themes/template/vendor/popper/umd/popper.min.js"></script>
<script src="themes/template/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="themes/template/vendor/common/common.min.js"></script>
<script src="themes/template/vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="themes/template/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="themes/template/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="themes/template/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="themes/template/vendor/isotope/jquery.isotope.min.js"></script>
<script src="themes/template/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="themes/template/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="themes/template/vendor/vide/jquery.vide.min.js"></script>
<script src="themes/template/vendor/vivus/vivus.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="themes/template/js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="themes/template/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="themes/template/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<!-- Current Page Vendor and Views -->
<script src="themes/template/js/views/view.contact.js"></script>

<!-- Theme Custom -->
<script src="themes/template/js/custom.js"></script>
<!-- Theme Initialization Files -->
<script src="themes/template/js/theme.init.js"></script>
