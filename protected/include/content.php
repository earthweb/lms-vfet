<div role="main" class="main">
	<div class="slider-container">
		<img src="themes/template/img/bg-login-2.jpg" alt="">
		<div class="container">
			<div class="row">

				<div class="offset-lg-7 col-lg-5 offset-sm-2 col-sm-8">
					<div class="login-first">
						<div class="signin-form">
							<h3 class="text-uppercase mb-4 font-weight-bold">เข้าสู่ระบบ</h3>
							<form>
								<div class="form-group">
									<label class="mb-1 text-2 opacity-8">Email address* </label>
									<input type="email" class="dr-e-from form-control form-control-lg">
								</div>
								<div class="form-group">
									<label class="mb-1 text-2 opacity-8">Password *</label>
									<!-- <input type="password" class="dr-e-from form-control form-control-lg"> -->
									<div class="input-group" id="show_hide_password">
										<input class="form-control" type="password">
										<div class="input-group-addon dr-e-from form-control-lg">
											<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
										</div>

									</div>
									<div class="form-row pb-2 pt-2">
										<div class="form-group form-check col-lg-6 pl-1">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="rememberMeCheck-2">
												<label class="custom-control-label text-2" for="rememberMeCheck-2">จำรหัสผ่าน</label>
											</div>
										</div>
										<div class="form-group col-lg-6 text-right">
											<a class="text-uppercase font-weight-bold text-color-dark" id="headerRecover" href="#">ลืมรหัสผ่าน</a>
										</div>
									</div>
									<div class="actions">
										<div class="form-row">
											<div class="col d-flex justify-content-end">
												<a class="btn btn-primary" href="#">เข้าสู่ระบบ</a>
											</div>
										</div>
									</div>
									<div class="extra-actions">
										<p>ท่านยังไม่มีบัญชี ? <a href="#" id="headerSignUp" class="text-uppercase font-weight-bold text-color-dark">ลงทะเบียนเรียน</a></p>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="home-intro mb-0" id="home-intro">
	<div class="container">

		<div class="row align-items-center">
			<div class="col-lg-8">
				<p>
					<span class="highlighted-word highlighted-word-animation-1 text-color-primary font-weight-semibold text-5 d-block">Dr.E-Course Cosmetic Surgery DIY</span>
					<span class="d-block mt-1"> ศัลยกรรมความงาม คุณทำได้เรียนรู้ ได้ทุกที่ ทุกเวลา ตั้งแต่พื้นฐาน จนลงมือทำเองได้</span>
				</p>
			</div>
			<div class="col-lg-4">
				<div class="get-started text-left text-lg-right">
					<span class="arrow hrb" style="top: -25px; left: 50px;" data-appear-animation="fadeInLeft" data-appear-animation-delay="0" data-appear-animation-duration="1s"></span>
					<a href="#" class="btn btn-primary btn-lg text-3 font-weight-semibold px-4 py-3">ดูรายละเอียดเพิ่มเติม</a>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="container">

	<div class="featured-boxes featured-boxes-style-4">
		<div class="featured-box featured-box-primary mt-5 mb-0">
			<div class="box-content">
				<i class="icon-featured fas fa-book-reader"></i>
				<h3 class="my-course">หลักสูตรของเรา</h3>
				<p class="about-course">จากประสบการณ์ ความชำนาญ กว่า 35 ปี ทั้งภาควิชาการ ปฏิบัติการ บริหารจัดการเราเข้าใจ<br>ถึงวิธีการถ่ายทอดความรู้ที่มีมาตรฐาน เห็นผลจริงในเวลาอันสั้น</p>
			</div>
		</div>
	</div>
	<div class="row">

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-1.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Nose Jobs</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-2.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Eyelid Plasty</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-3.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Charming Smile</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-4.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">V-Face Lift</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-5.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Fat Grafting</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-6.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Breast Implantation</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-7.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Fillers and Botulinum Rx</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

		<div class="col-lg-3">
			<div class="course-box">
				<span class="thumb-info thumb-info-no-borders thumb-info-no-borders-rounded thumb-info-lighten thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom">
					<span class="thumb-info-wrapper">
						<img src="themes/template/img/course-8.jpg" class="img-fluid" alt="">
						<span class="thumb-info-title">
							<span class="thumb-info-inner line-height-1">Beauty Devices</span>
							<!-- <span class="thumb-info-type opacity-8">Project Type</span> -->
							<span class="thumb-info-show-more-content opacity-7">
								<p class="mb-0 text-1 line-height-5 mt-1">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
							</span>
						</span>
					</span>
				</span>
			</div>
		</div>

	</div>

</div>

<hr class="pattern tall">