<section class="section bg-primary border-0 m-0 mt-4">
	<div class="container">
		<div class="row justify-content-center text-center text-lg-left py-4">
			<div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter">
				<div class="feature-box feature-box-style-2 d-block d-lg-flex mb-4 mb-lg-0">
					<div class="feature-box-icon">
						<i class="icon-location-pin icons text-color-light"></i>
					</div>
					<div class="feature-box-info pl-1">
						<h5 class="font-weight-light text-color-light opacity-7 mb-0">ที่อยู่</h5>
						<p class="text-color-light font-weight-semibold mb-0">174. ซ. ศรีบรูพา6 ถ. ศรีบรูพา เขตบางกะปิ 10240</p>
					</div>
				</div>
			</div>
			<div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
				<div class="feature-box feature-box-style-2 d-block d-lg-flex mb-4 mb-lg-0 px-xl-4 mx-lg-5">
					<div class="feature-box-icon">
						<i class="fab fa-line icons text-color-light"></i>
					</div>
					<div class="feature-box-info pl-1">
						<h5 class="font-weight-light text-color-light opacity-7 mb-0">ติดต่อเรา</h5>
						<a href="#" class="text-color-light font-weight-semibold text-decoration-none">
							 dr.v-fet<!-- <img src="themes/template/img/line.jpg" alt=""> -->
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
				<div class="feature-box feature-box-style-2 d-block d-lg-flex">
					<div class="feature-box-icon">
						<i class="icon-share icons text-color-light"></i>
					</div>
					<div class="feature-box-info pl-1">
						<h5 class="font-weight-light text-color-light opacity-7 mb-0">ติดตามเรา</h5>
						<p class="mb-0">
							<span class="social-icons-facebook"><a href="https://www.facebook.com/kochakorn888/" target="_blank" class="text-color-light font-weight-semibold" title="Facebook"><i class="mr-1 fab fa-facebook-f"></i> Facebook</a></span>
							<span class="social-icons-twitter pl-3"><a href="http://www.vfetclinic.com" target="_blank" class="text-color-light font-weight-semibold" title="Website"><i class="mr-1 fas fa-globe"></i> Website</a></span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>

<footer id="footer" class="mt-0">
	<div class="footer-copyright">
		<div class="container ">
			<div class="row py-3">
				<div class="col d-flex align-items-center justify-content-center">
					<p><strong>Dr.E V FET</strong> - © Copyright 2019. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>