        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

        <!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
		<link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="themes/template/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="themes/template/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="themes/template/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="themes/template/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="themes/template/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="themes/template/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="themes/template/vendor/magnific-popup/magnific-popup.min.css">



		<!-- Theme CSS -->
		<link rel="stylesheet" href="themes/template/css/theme.css">
		<link rel="stylesheet" href="themes/template/css/theme-elements.css">
		<link rel="stylesheet" href="themes/template/css/theme-blog.css">
		<link rel="stylesheet" href="themes/template/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="themes/template/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="themes/template/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="themes/template/vendor/rs-plugin/css/navigation.css">
		<link rel="stylesheet" href="themes/template/vendor/circle-flip-slideshow/css/component.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="themes/template/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="themes/template/css/custom.css">

		<!-- Head Libs -->
		<script src="themes/template/vendor/modernizr/modernizr.min.js"></script>