<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
	<div class="header-body">
		<div class="header-container container">
			<div class="header-row">
				<div class="header-column">
					<div class="header-row">
						<div class="header-logo">
							<a href="#">
								<img alt="Dr.E" height="90" data-sticky-height="50" data-sticky-top="0" src="themes/template/img/logo-1.png">
							</a>
						</div>
					</div>
				</div>
				<div class="header-column justify-content-end">
					<div class="header-row">
						<div class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
							<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
								<nav class="collapse">
									<ul class="nav nav-pills" id="mainNav">
										<li class="dropdown ">
											<a class="dropdown-item dropdown-toggle active" href="#">
												หน้าหลัก
											</a>
										</li>
										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
												Dr.E-Course
											</a>
											<ul class="dropdown-menu">
												<li><a class="dropdown-item" href="#">Nose Jobs </a></li>
												<li><a class="dropdown-item" href="#">Eyelid Plasty </a></li>
												<li><a class="dropdown-item" href="#">Charming Smile</a></li>
												<li><a class="dropdown-item" href="#">V-Face Lift</a></li>
												<li><a class="dropdown-item" href="#">Fat Grafting</a></li>
												<li><a class="dropdown-item" href="#">Breast Implantation</a></li>
												<li><a class="dropdown-item" href="#">Fillers and Botulinum Rx</a></li>
												<li><a class="dropdown-item" href="#">Beauty Devices</a></li>
											</ul>
										</li>

										<li class="dropdown">
											<a class="dropdown-item dropdown-toggle" href="#">
												ติดต่อเรา
											</a>
											<!-- <ul class="dropdown-menu">
												<li><a class="dropdown-item" href="#">Welcome </a></li>
												<li><a class="dropdown-item" href="#">Venue</a></li>
											</ul> -->
										</li>

										<li class="dropdown freetrial">
											<a class="dropdown-item dropdown-toggle" href="#">
												เกี่ยวกับเรา
											</a>
										</li>

									</ul>
								</nav>
							</div>
							<div class="header-nav-features">
								<div class="header-nav-features-search-reveal-container">
									<div class="header-nav-feature header-nav-features-search header-nav-features-search-reveal d-inline-flex">
										<a href="#" class="header-nav-features-search-show-icon d-inline-flex"><i class="fas fa-search header-nav-top-icon"></i></a>
									</div>
								</div>
							</div>
							<div class="header-nav-features header-nav-features-no-border p-static z-index-2">
								<div class="header-nav-feature header-nav-features-search header-nav-features-search-reveal header-nav-features-search-reveal-big-search px-3">
									<form role="search" class="d-flex w-100 h-100" action="page-search-results.html" method="get">
										<div class="big-search-header input-group">
											<input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="พิมพ์คำค้นหาและกด Enter...">
											<a href="#" class="header-nav-features-search-hide-icon"><i class="fas fa-times header-nav-top-icon"></i></a>
										</div>
									</form>
								</div>
							</div>

							<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
								<i class="fas fa-bars"></i>
							</button>
						</div>

						<div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">

							<div class="header-nav-feature header-nav-features-user d-inline-flex mx-2 pr-2 signin" id="headerAccount">
								<a href="#" target="_blank" class="btn btn-lg btn-outline btn-dark font-weight-semibold line-height-1  d-none d-md-inline-block ">วิธีชำระเงิน</a>
								<a href="#" target="_blank" class="btn btn-lg btn-primary font-weight-semibold line-height-1 ml-2 d-none d-sm-inline-block header-nav-features-toggle">เข้าสู่ระบบ | ลงทะเบียน</a>
								<!-- <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed header-nav-features-dropdown-force-right" id="headerTopUserDropdown">
									<div class="signin-form">
										<h5 class="text-uppercase mb-4 font-weight-bold text-3">Sign In</h5>
										<form>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Email address* </label>
												<input type="email" class="form-control form-control-lg">
											</div>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Password *</label>
												<input type="password" class="form-control form-control-lg">
											</div>
											<div class="form-row pb-2">
												<div class="form-group form-check col-lg-6 pl-1">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" id="rememberMeCheck">
														<label class="custom-control-label text-2" for="rememberMeCheck">Remember Me</label>
													</div>
												</div>
												<div class="form-group col-lg-6 text-right">
													<a class="text-uppercase font-weight-bold text-color-dark" id="headerRecover" href="#">LOST YOUR PASSWORD?</a>
												</div>
											</div>
											<div class="actions">
												<div class="form-row">
													<div class="col d-flex justify-content-end">
														<a class="btn btn-primary" href="#">Login</a>
													</div>
												</div>
											</div>
											<div class="extra-actions">
												<p>Don't have an account yet? <a href="#" id="headerSignUp" class="text-uppercase  font-weight-bold text-color-dark">ลงทะเบียนเรียน</a></p>
											</div>
										</form>
									</div> -->

									<!-- <div class="signup-form">
										<h5 class="text-uppercase mb-4 font-weight-bold text-3">ลงทะเบียนเรียน</h5>
										<form>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Email address* </label>
												<input type="email" class="form-control form-control-lg">
											</div>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Password *</label>
												<input type="password" class="form-control form-control-lg">
											</div>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Re-enter Password *</label>
												<input type="password" class="form-control form-control-lg">
											</div>
											<div class="actions">
												<div class="form-row">
													<div class="col d-flex justify-content-end">
														<a class="btn btn-primary" href="#">Register</a>
													</div>
												</div>
											</div>
											<div class="extra-actions">
												<p>Already have an account? <a href="#" id="headerSignIn" class="text-uppercase text-1 font-weight-bold text-color-dark">Log In</a></p>
											</div>
										</form>
									</div> -->

									<!-- <div class="recover-form">
										<h5 class="text-uppercase mb-2 pb-1 font-weight-bold text-3">Reset My Password</h5>
										<p class="text-2 mb-4">Complete the form below to receive an email with the authorization code needed to reset your password.</p>

										<form>
											<div class="form-group">
												<label class="mb-1 text-2 opacity-8">Email address* </label>
												<input type="email" class="form-control form-control-lg">
											</div>
											<div class="actions">
												<div class="form-row">
													<div class="col d-flex justify-content-end">
														<a class="btn btn-primary" href="#">Reset</a>
													</div>
												</div>
											</div>
											<div class="extra-actions">
												<p>Already have an account? <a href="#" id="headerRecoverCancel" class="text-uppercase text-1 font-weight-bold text-color-dark">Log In</a></p>
											</div>
										</form>
									</div> -->

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>